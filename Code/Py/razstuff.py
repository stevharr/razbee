#razstuff

from __future__ import print_function
import datetime
import RPi.GPIO as GPIO
import httplib
import re
import select
import serial
import string
import sys
import termios
import thread
import time
import tty
import xml.dom.minidom

#color output
#TODO: CHECK OUT COLORAMA!!
#to use colors: cxxx.format("text to be in cxxx color")
cblk = "\033[22;30m{0}\033[00m" # black
cdgry = "\033[01;30m{0}\033[00m" # dark gray
cgry = "\033[22;37m{0}\033[00m" # gray
cwht = "\033[01;37m{0}\033[00m" # white

cbrn = "\033[22;33m{0}\033[00m" # brown

cred = "\033[22;31m{0}\033[00m" # red
cgrn = "\033[22;32m{0}\033[00m" # green
cblu = "\033[22;34m{0}\033[00m" # blue
clred = "\033[01;31m{0}\033[00m" # light red
clgrn = "\033[01;32m{0}\033[00m" # light green
clblu = "\033[01;34m{0}\033[00m" # light blue

ccyn = "\033[22;36m{0}\033[00m" # cyan
cmag = "\033[22;35m{0}\033[00m" # magenta
cyel = "\033[01;33m{0}\033[00m" # yellow
clcyn = "\033[01;36m{0}\033[00m" # light cyan
clmag = "\033[01;35m{0}\033[00m" # light magenta

#To see the resulting colors and their names, uncomment the next few lines. . .
#color = cdgry.format("cdgry") + cgry.format("cgry") + cwht.format("cwht") + cbrn.format("cbrn") + \
#        cred.format("cred") + cgrn.format("cgrn") + cblu.format("cblu") + clred.format("clred") + \
#        clgrn.format("clgrn") + clblu.format("clblu") + ccyn.format("ccyn") + cmag.format("cmag") + \
#        cyel.format("cyel") + clcyn.format("clcyn") + clmag.format("clmag")
#print(color)

class WxThing:
    def __init__(self, ini, cosmkey, sensekey, starttime): 
        self.loc = ini.get("weather", "station")
        self.name = ini.get("weather", "name")
        self.feed = ini.get("weather", "cosmfeed")
        self.rate = int(ini.get("weather", "rate"))
        self.cosm = Cosm(cosmkey, self.feed) #for cosm posting

        self.nextiotpost = starttime - datetime.timedelta(seconds=self.rate) #set initial next post to be immediate
        self.previotpost = starttime

        self.sense = SenSe(sensekey)

        self.wt = ini.get("weather", "sensetemperature")
        self.wh = ini.get("weather", "sensehumidity")
        self.wwd = ini.get("weather", "sensewdir")
        self.wws = ini.get("weather", "sensewspeed")
        self.wp = ini.get("weather", "sensepressure")


class RazThing: 
    def __init__(self, id, ini, cosmkey, sensekey, starttime): 
        self.id = id

        thing = "razthing-" + id
        self.rtname = ini.get(thing, "name")
        self.rtfeed = ini.get(thing, "cosmfeed")
        self.rate = int(ini.get(thing, "rate"))

        self.cosm = Cosm(cosmkey, self.rtfeed)
        self.msgcount = 0
        self.nextiotpost = starttime + datetime.timedelta(seconds=self.rate)
        self.previotpost = starttime

        self.at = AvgList() #create a list to track and average temperature readings
        self.av = AvgList() #create a list to track and average battery voltages
        self.ar = AvgList() #create a list to track and average RSSI values
        self.ad = AvgList() #create a list to track and average message deltas
        self.ax = AvgList() #create a list to track x axis values
        self.ay = AvgList() #create a list to track x axis values        
        self.az = AvgList() #create a list to track x axis values
        self.ast = AvgList() #create a list to watch for changing accel status
        self.ao = AvgList() #create a list to watch for changing accel orientation
        self.au = AvgList() #create a list to track and average ultrasound readings
        self.al = AvgList() #create a list to track and average light readings

        self.seq = SeqCheck(255) #to check for consecutive sequence numbers    
        self.lastgaptime = "never"

        self.prevmsgtime = starttime #we'll use these to calculate time between messages
        self.currmsgtime = starttime
        
        self.sense = SenSe(sensekey)
        self.ft = ini.get(thing, "sensetemperature")
        self.fv = ini.get(thing, "sensebattery")
        self.fr = ini.get(thing, "senserssi")
        self.fd = ini.get(thing, "sensemsgdelta")
        self.fx = ini.get(thing, "senseaccelx")
        self.fy = ini.get(thing, "senseaccely")
        self.fz = ini.get(thing, "senseaccelz")
        self.fs = ini.get(thing, "senseaccelstat")
        self.fo = ini.get(thing, "senseorientation")
        self.fu = ini.get(thing, "senseultrasound")
        self.fl = ini.get(thing, "senselight")
        
        
class RazBeeRadio:
    # open and initialize the serial port
    # the speed is specified by the caller
    def __init__(self, speed, reset):
        if reset:
            self.resetRazbeeRadio() #reset the MC1322x radio chip on the RazBee

        self.ser = serial.Serial("/dev/ttyAMA0")
        self.ser.baudrate = speed
        self.ser.bytesize = 8
        self.ser.parity = "N"
        self.ser.stopbits = 1
        self.ser.timeout = 1
        self.ser.xonxoff = 0
        self.ser.rtscts = 0
        print("Opened serial port " + self.ser.portstr + " @ " + str(self.ser.baudrate))
        self.send_raw("\xfd\x07")
        
    # close closes the serial port (bonus points if you figured that out yourself)
    def close(self):
        self.ser.close()
        print("Closed serial port")

    # send_raw sends a message to the serial port
    def send_raw(self, msg):
        msg = msg 
        self.ser.write(msg)
        print("Sent raw data [" + msg.encode("hex") + "]") #we're done
        
    #recv reads at most a requested number of bytes from the serial port
    def recv(self, length):
        while 1: 
            response = self.ser.read(length)
            if len(response) > 0:
                break;
        return response
        
    #try to get the requested bytes of sensor data
    def get_sensor_data(self, sensorlen):
        msg = ""
        i,o,e = select.select([self.ser],[],[],0.0001)
        for s in i:
            if s == self.ser:
                msg = self.recv(sensorlen)
                        
        return msg

    #reset the MC1322x radio chip on the RazBee 
    def resetRazbeeRadio(self): 
        pin = 12 #GPIO-1 is RPi pin 12 (BCM pin 18)
        value = 0
        states = [GPIO.HIGH, GPIO.HIGH, GPIO.LOW, GPIO.HIGH, GPIO.HIGH]
        print(cwht.format("Resetting MC1322x Radio Chip on RazBee"))
        #Set the mode numbering of the pins
        GPIO.setmode(GPIO.BOARD)  #if using GPIO.BCM, use pin=18 for GPIO-1
        GPIO.setwarnings(False)

        GPIO.setup(pin, GPIO.OUT)
        value = GPIO.input(pin)
        print("Beginning state of GPIO-x (pin = " + str(pin) + ") is " + str(value))
        
        for state in states:
            GPIO.output(pin, state)
            value = GPIO.input(pin)
            print("Current state of GPIO-x (pin = " + str(pin) + ") is " + str(value))
            time.sleep(1)

    
    def parse(self, msg):
        #converts the data from binary to ascii-hex, then converts the values back to binary
        #this isn't the most efficient for dealing with the data for display by this program, but 
        # since it will end up sending the data somewhere else (probably XML), then this is probably ok
        #some values, like temperature and battery voltage, need some extra attention to decode them properly
        msgasc = msg.encode("hex") #convert the data to ascii-hex
        
        msglen = len(msg)
        
        #get the individual data fields from the ascii-hex string
        xrbsequence = msgasc[6:8]
        xrbthingid = msgasc[8:12]
        xrbxaxis = msgasc[12:14]
        xrbyaxis = msgasc[14:16]
        xrbzaxis = msgasc[16:18]
        xrbaccstat = msgasc[18:20]
        xrbtempmsb = msgasc[22:24]
        xrbtemplsb = msgasc[24:26]
        xrbbatmsb = msgasc[26:28]
        xrbbatlsb = msgasc[28:30]
        xrblqi = msgasc[30:32]
        
        if msglen == 25: #if firmware 9.3 or later
            xrbusmsb = msgasc[32:34]
            xrbuslsb = msgasc[34:36]
            xrblight = msgasc[36:38]
        else:
            xrbusmsb = "00"
            xrbuslsb = "00"
            xrblight = "00"
        
        #for internal use by this program, convert to binary values
        sequence = self.hex2dec(xrbsequence)

        xaxis = self.hex2dec(xrbxaxis)
        if xaxis > 127:
            xaxis = -(xaxis - 128)
            xneg = 1
        else:
            xneg = 0
        if xaxis < 0:
            xaxis += 128

        yaxis = self.hex2dec(xrbyaxis)
        if yaxis > 127:
            yaxis = -(yaxis - 128)
            yneg = 1
        else:
            yneg = 0
        if yaxis < 0:
            yaxis += 128

        zaxis = self.hex2dec(xrbzaxis)
        if zaxis > 127:
            zaxis = -(zaxis - 128)
            zneg = 1
        else:
            zneg = 0
        if zaxis < 0:
            zaxis += 128
            
        accorient = (zneg * 4) + (yneg * 2) + xneg #bitmap of orientation

        accstat = self.hex2dec(xrbaccstat) 
        accstat += (accorient * 256) 

        temperature = self.ctof(self.makefloat(xrbtempmsb, xrbtemplsb))
        batvolt = self.makefloat(xrbbatmsb, xrbbatlsb)
        lqi = self.hex2dec(xrblqi)
        rssi = self.lqi2rssi(lqi)
        ultrasound = (self.hex2dec(xrbusmsb) * 256) + self.hex2dec(xrbuslsb)
        light = self.hex2dec(xrblight)
        
        return sequence, xrbthingid, xaxis, yaxis, zaxis, accstat, temperature, batvolt, rssi, ultrasound, light


    def lqi2rssi(self, lqi): #convert Freescale LQI to RSSI (formula per Steve D)
        rssi = (-15) - ((85.0/255.0) * (255-lqi))
        return rssi
        
    def makefloat(self, ahmsb, ahlsb): #convert two bytes to a float: msb is signed integer part, lsb is decimal part
        binmsb = self.hex2dec(ahmsb)
        binlsb = self.hex2dec(ahlsb)
        if binmsb > 127:
            binmsb -= 128
            sign = -1
        else:
            sign = 1
        binlsb /= 100.0
        floatval = (binmsb + binlsb) * sign
        return floatval
        
    def ctof(self, celsius): #convert celcius to fahrenheit
        return float((celsius * 1.8) + 32) #convert to fahrenheit
        
    def hex2dec(self, s):  #return the integer value of a hexadecimal string (ie, "0D" returns 13)
        return int(s, 16)


class Cosm: #init and post to a cosm.com feed
    def __init__(self, key, feed): #caller provides cosm api-key and feed number
        self.api_key = key
        self.feed = feed

        self.baseurl = "api.cosm.com"
        self.feedurl = "/v2/feeds/%s.json" % str(self.feed)

    #pass the data to a new function if a different thread so we don't get blocked while the data is posted
    def post(self, data):
        if len(data) > 0 and int(self.feed) > 0:
            try:
               thread.start_new_thread(self.post_thread, (data,))
            except Exception as e:
               print(clred.format("Error: unable to start Cosm post thread: " + str(e)))

    def post_thread(self, data):
        print(clgrn.format("POSTING DATA AT COSM"))
        data = json("cosm", data)
        headers = {"Content-type":"application/json","X-ApiKey":self.api_key,"Content-length":len(data)}  
        conn = httplib.HTTPConnection(self.baseurl)
        conn.request("PUT", self.feedurl, data, headers)
        try:
            response = conn.getresponse()
        except Exception as e:
           print(clred.format("Error getting response from Cosm: " + str(e)))
        status = int(response.status)
        if status == 200:
            pass
            #print(clgrn.format(" COSM DATA POSTED OK"))
        else:
            print(clred.format("ERROR " +  str(response.status) + " POSTING TO COSM: " + response.reason))
            data = response.read()
            print(cred.format(data))
        conn.close()


class SenSe: #init and post to a sen.se feed

    def __init__(self, api_key):
        self.api_key = api_key
        
        self.base_url = "api.sen.se"
        self.url = "/events/"

    #pass the data to a new function if a different thread so we don't get blocked while the data is posted
    def post(self, data):
        if len(data) > 0:
            try:
               thread.start_new_thread(self.post_thread, (data,))
            except Exception as e:
               print(clred.format("Error: unable to start sen.se post thread: " + str(e)))

    def post_thread(self, data):
        print(clgrn.format("POSTING DATA AT SEN.SE"))
        data = json("sense", data)
        headers = {"Content-type":"application/json","sense_key":self.api_key,"Content-length":len(data)}
        conn = httplib.HTTPConnection(self.base_url)
        conn.request("POST", self.url, data, headers)
        try:
            response = conn.getresponse()
        except Exception as e:
           print(clred.format("Error getting response from Sen.se: " + str(e)))
        status = int(response.status)
        if status == 200:
            pass
            #print(clgrn.format(" SEN.SE DATA POSTED OK"))
        else:
            print(clred.format("ERROR " +  str(response.status) + " POSTING TO SEN.SE: " + response.reason))
            data = response.read()
            print(cred.format(data))
        conn.close()
    
  
class AvgList:  #creates a rolling list, tracking the last X values and calculating the average of those values

    def __init__(self): #create a list for averaging values
        self.initval = 999999
        self.previous = self.initval
        self.current = self.initval
        self.clear()
        self.maximum = -self.initval
        self.maxtime = self.start
        self.minimum = self.initval
        self.mintime = self.start
        
    #add an item to the list
    def add(self, newval): 
        self.rlist.append(newval)

        if newval > self.maximum: #check for new max value
            self.maximum = newval
            self.maxtime = datetime.datetime.now()
        if newval < self.minimum: #check for new min value
            self.minimum = newval
            self.mintime = datetime.datetime.now()
            
        self.previous = self.current
        self.current = newval

    #test if the most recent two values added to the list are contiguous
    def isContig(self):
        if abs(self.current - self.previous) <= 1:
            return True
        else:
            return False

    #test if the most recent two values are different
    def isDifferent(self):
        return (self.current != self.previous)

    #test if the most recent two values are the same        
    def isSame(self):
        return (self.current == self.previous)

    #return the average of the values in the list
    def average(self, decimals):
        if len(self.rlist) > 0: #avoid a divide by zero error!
            self.avg = round(float(sum(self.rlist)) / len(self.rlist), decimals)
        else:
            self.avg = 0.0
        return self.avg

    #clear all the data from the list
    def clear(self):
        self.rlist = list()
        self.start = datetime.datetime.now()
        self.avg = 0

        
class SeqCheck: #verifies that consecutive calls to check() are contiguous values without gaps
    def __init__(self, max):
        self.seqmax = max
        self.shouldbe = self.seqmax + 1
        self.init = False
        self.gaps = 0

    def check(self, got): #
        if self.init == True: #if we've received an initial value
            retval = got - self.shouldbe #should be zero if there's no gap in the sequence
            if retval != 0:
                self.gaps += 1 #count the number times we've had gaps in sequences
        else:
            self.init = True #we just got our initial value
            retval = 0 #so the return value is 0 ("no gap")

        self.shouldbe = got + 1 #the next number "should be" the current value plus one
        if self.shouldbe > self.seqmax: #if the next number exceeds the rollover value
            self.shouldbe = 0 #reset the sequence number
        return retval
        
    def gaps(self): #returns the number of times we've had gaps in the sequence (gaps, not the number of missing packets!)
        return self.gaps


# returns true if there's data from the keyboard (ending with <enter>)        
def kbd_ready(): 
        i,o,e = select.select([sys.stdin],[],[],0.0001)
        for s in i:
            if s == sys.stdin:
                input = sys.stdin.readline()
                return input #return what was entered
        return "" #return empty string if no keyboard input

        
#make sure Cosm Datastream names are valid (A-Z a-z 0-9 _)
def clean_ds_name(name, tc=False): 
    if tc:
        name = name.title() #Title Case Each Word If Requested
    name = name.replace(" ","_") #replace_spaces_with_underscores
    name = re.sub(r'\W+','',name) #remove all non alphanumeric characters
    return name

#return 8 values representing the state of each bit in a byte    
def get_bits(byte): 
    return list(bin(byte)[2:].rjust(8,'0'))  #start at offset 2 to remove the "0b" prefix returned by bin()


#get razthing info from config file and return a dictionary of RazThing objects
def get_razthings(ini, cosmkey, sensekey, starttime): 
    rtlist = {}
    razthings = ini.options("razthings")
    for num in razthings:
        id = ini.get("razthings", str(num))
        rtlist[id] = RazThing(id, ini, cosmkey, sensekey, starttime)
        
    return rtlist


#start a thread to read the NOAA weather
def get_local_weather(station, postToCosm, postToSense):
    if station != "NONE":
        try:
           thread.start_new_thread(get_local_weather_thread, (station, postToCosm, postToSense))
        except Exception as e:
           print(clred.format("Error: unable to start a get_local_weather thread: " + str(e)))

           
#read the weather info from NOAA and post relevant data to Cosm and Sense
def get_local_weather_thread(wx, postToCosm, postToSense):
    wxurl = "w1.weather.gov"
    wxpage = "/xml/current_obs/%s.xml" % wx.loc
    print(clblu.format("Getting local weather"))
    conn = httplib.HTTPConnection(wxurl)
    conn.request("GET", wxpage)
    response = conn.getresponse()
    status = int(response.status)
    if status == 200: #we got a good reply from NOAA, so we'll read the data, parse it, and post it
        data = response.read()
        wt, wh, wwd, wws, wp = parse_weather_xml(data)
        wxdisplay = "%s : Temperature %s, Humidity %s, Wind %s mph from %s, Pressure %s" % (wx.loc, wt, wh, wws, wwd, wp)
        print(clblu.format(wxdisplay))
        if postToCosm:
            print(cgrn.format("Sending data to COSM for ") + clgrn.format(wx.name)) 
            wxdata = []
            wxdata.append((wx.loc + "_temperature", str(wt))) 
            wxdata.append((wx.loc + "_humidity", str(wh)))
            wxdata.append((wx.loc + "_wind_speed", str(wws)))
            wxdata.append((wx.loc + "_wind_direction", str(wwd)))
            wxdata.append((wx.loc + "_pressure", str(wp)))
            wx.cosm.post(wxdata) #send the data to Cosm

        if postToSense:
            if int(wx.wt) > 0:
                print(cgrn.format("Sending data to sen.se for ") + clgrn.format(wx.name))
                wxdata = []
                if wx.wt != "0":
                    wxdata.append((wx.wt, str(wt))) 
                if wx.wh != "0":
                    wxdata.append((wx.wh, str(wh)))
                if wx.wp != "0":
                    wxdata.append((wx.wp, str(wp)))
                if wx.wws != "0":
                    wxdata.append((wx.wws, str(wws)))
                if wx.wwd != "0":
                    wxdata.append((wx.wwd, str(wwd)))
                    
                if len(wxdata) > 0:
                    wx.sense.post(wxdata) #send the data to SenSe
        
    else:
        print(clred.format("ERROR " +  str(response.status) + " getting local weather: " + response.reason))

    conn.close()

    
#parse the XML data we got from NOAA and return relevant values    
def parse_weather_xml(data):
    wxdom = xml.dom.minidom.parseString(data) 

    wxtempf = wxdom.getElementsByTagName("temp_f")[0].childNodes[0].data
    wxhumid = wxdom.getElementsByTagName("relative_humidity")[0].childNodes[0].data
    wxwdeg = wxdom.getElementsByTagName("wind_degrees")[0].childNodes[0].data
    wxwmph = wxdom.getElementsByTagName("wind_mph")[0].childNodes[0].data
    wxpress = wxdom.getElementsByTagName("pressure_in")[0].childNodes[0].data
    return wxtempf, wxhumid, wxwdeg, wxwmph, wxpress


#based on the 'service' (cosm or sense) create JSON formatted data from the list of tuples
def json(service, data):
    jsonstring = ""
    comma = ""
    nocomma = True
    if service.lower() == "cosm": 
        jstr = """%s{"id" : "%s","current_value":"%s"}"""
    elif service.lower() == "sense":
        jstr = """%s{"feed_id": "%s","value": "%s"}"""
        
    for d in data:
        jsonstring += jstr % (comma, str(d[0]), str(d[1]))
        if nocomma:
            comma = ","
            nocomma = False

    if len(jsonstring) > 0:
        if service == "cosm":
            jsonstring = """ {"datastreams" : ["""  + jsonstring + "]}"
        else:
            jsonstring = "["  + jsonstring + "]"
        
    return jsonstring
        
