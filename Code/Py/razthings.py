#usr/bin/env python

#razthings.py
# display incoming remote sensor data from a RazThing
#  and post data to Cosm.com    

from __future__ import print_function
import atexit
from ConfigParser import SafeConfigParser
import datetime
import signal
import sys

###########################################################
# LJ DIMMING DEMO
import socket
###########################################################

import razstuff


RAZTHINGS_VERSION = "20130123a"  #<===== KEEP THIS VERSION UPDATED!
PACKETLEN = 25

def ctrlC_handler(signal, frame): #gracefully handle the ctrl-C
    print(clred.format(" ctrl-C was pressed.  Exiting. . ."))
    sys.exit(0)

def byebye(): #clean up and exit
    now = datetime.datetime.now()
    try:
        rb.close() #close the serial port
    except:
        pass #hadn't opened yet. . .
        
###########################################################
# LJ DIMMING DEMO
    stop_ljdemo()
###########################################################

    print(clgrn.format("Exiting.  Ran for " + str(now - starttime)))
    print(clgrn.format("Done. . ."))
    #program ends

###########################################################
# LJ DIMMING DEMO
def tell_ljmon(msg):  #send a message to LJ Monitor
    try:
        sockljmon.send(msg + "\r\n")
    except socket.error, msg:
        print(cred.format("Error sending LJMon = " + str(msg[0]) + ": " + msg[1]))

def start_ljdemo():
    global sockljmon
    print(" Opening socket. . .")
    try:
        sockljmon = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error, msg:
        print(clred.format("Failed to create socket. Error " + str(msg[0]) + ": " + msg[1]))
        sys.exit(1)
    print(" Connecting to socket. . .")
    try:
        sockljmon.connect((ljmonhost,ljmonport))
    except socket.error, msg:
        print(clred.format("Couldn't connect to port " + str(ljmonport) + ". Error " + str(msg[0]) + ": " + msg[1]))
        sys.exit(1)

def stop_ljdemo():
    print(" Closing socket. . .")
    try:
        sockljmon.close()  #close the socket to LJ
    except:
        pass #hadn't opened yet
###########################################################

#color output
#TODO: CHECK OUT COLORAMA!!
#to use colors: cxxx.format("text to be in cxxx color")
cblk = "\033[22;30m{0}\033[00m" # black
cdgry = "\033[01;30m{0}\033[00m" # dark gray
cgry = "\033[22;37m{0}\033[00m" # gray
cwht = "\033[01;37m{0}\033[00m" # white

cbrn = "\033[22;33m{0}\033[00m" # brown

cred = "\033[22;31m{0}\033[00m" # red
cgrn = "\033[22;32m{0}\033[00m" # green
cblu = "\033[22;34m{0}\033[00m" # blue
clred = "\033[01;31m{0}\033[00m" # light red
clgrn = "\033[01;32m{0}\033[00m" # light green
clblu = "\033[01;34m{0}\033[00m" # light blue

ccyn = "\033[22;36m{0}\033[00m" # cyan
cmag = "\033[22;35m{0}\033[00m" # magenta
cyel = "\033[01;33m{0}\033[00m" # yellow
clcyn = "\033[01;36m{0}\033[00m" # light cyan
clmag = "\033[01;35m{0}\033[00m" # light magenta

#To see the resulting colors and their names, uncomment the next few lines. . .
#color = cdgry.format("cdgry") + cgry.format("cgry") + cwht.format("cwht") + cbrn.format("cbrn") + \
#        cred.format("cred") + cgrn.format("cgrn") + cblu.format("cblu") + clred.format("clred") + \
#        clgrn.format("clgrn") + clblu.format("clblu") + ccyn.format("ccyn") + cmag.format("cmag") + \
#        cyel.format("cyel") + clcyn.format("clcyn") + clmag.format("clmag")
#print(color)
   
    
### PROGRAM BEGINS HERE ###
starttime = datetime.datetime.now()

signal.signal(signal.SIGINT, ctrlC_handler)  #trap ctrl-c for a cleaner exit
atexit.register(byebye)  #to close up neatly we'll call byebye() when the script is exiting

print(cyel.format("razthings V" + RAZTHINGS_VERSION))
print(clgrn.format("Starting " + str(starttime)))

#init serial port and RazBee radio
rb = razstuff.RazBeeRadio(38400, True) 

#get parameters from the config file
ini = SafeConfigParser()
ini.read("razthings.ini")

#get Cosm setup info
cosmkey = ini.get("cosm", "apikey")
axname = ini.get("cosmfeednames", "accelx")
ayname = ini.get("cosmfeednames", "accely")
azname = ini.get("cosmfeednames", "accelz")
asname = ini.get("cosmfeednames", "accelstat")
aoname = ini.get("cosmfeednames", "orientation")
btname = ini.get("cosmfeednames", "battery")
mdname = ini.get("cosmfeednames", "msgdelta")
rsname = ini.get("cosmfeednames", "rssi")
tpname = ini.get("cosmfeednames", "temperature")
usname = ini.get("cosmfeednames", "ultrasound")
ltname = ini.get("cosmfeednames", "light")
postToCosm = ini.get("cosm", "post") == "yes"
    
#get Sen.se setup info
postToSense = ini.get("sense", "post")
sensekey = ini.get("sense", "apikey")
    
#make a RazThing dictionary (lookup by rtid)
rt = {}
rt = razstuff.get_razthings(ini, cosmkey, sensekey, starttime)

#set up the weather station info
#TODO: this could be a list to get data from more than NOAA weather station
wx = razstuff.WxThing(ini, cosmkey, sensekey, starttime)

###########################################################
# LJ DIMMING DEMO
ljrtid = ini.get("ljdemo", "rtid") #the RazThing that controls the demo
ljxload = ini.get("ljdemo", "loadx") #the LJ load controlled by the X axis
ljyload = ini.get("ljdemo", "loady") #the LJ load controlled by the Y axis
ljmonhost = ini.get("ljdemo", "host") #the host ip address of the LJ monitor program
ljmonport = int(ini.get("ljdemo", "port")) #the port of the LJ monitor program
###########################################################

#In the main loop:    
# pressing X<enter> will terminate the loop (and the program)
# pressing P<enter> will toggle display of the packet data
# pressing R<enter> will toggle display of the raw data
# pressing T<enter> will toggle display of the temperature data
# pressing G<enter> will toggle display of the gap info
# pressing A<enter> will toggle display of the axis/accel info
# pressing V<enter> will toggle display of the voltage info
# pressing S<enter> will toggle display of the signal strength info
# pressing M<enter> will toggle display of the message delta average
# pressing U<enter> will toggle display of the ultrasound data
# pressing L<enter> will toggle display of the light sensor data
# pressing C<enter> will toggle posting to Cosm
# pressing N<enter> will toggle posting to Sen.se
# pressing J<enter> will toggle light dimming demo on attached LiteJet (don't use without an attached LiteJet system)
#  set defaults here
showpacket = ini.get("display","packet") == "yes"
showraw = ini.get("display", "raw") == "yes"
showtemp = ini.get("display", "temperature") == "yes"
showgap = ini.get("display", "gap") == "yes"
showaxis = ini.get("display", "axis") == "yes"
showvolt = ini.get("display", "voltage") == "yes"
showsig = ini.get("display", "signal") == "yes"
showmd = ini.get("display", "msgdelta") == "yes"
showus = ini.get("display", "ultrasound") == "yes"
showlight = ini.get("display", "light") == "yes"

###########################################################
# LJ DIMMING DEMO
ljdemo = False 
###########################################################

print(clgrn.format("And away we go!"))
print(clgrn.format("Press X<enter> to exit or P(acket), R(aw), T(emperature), G(aps), A(xis), V(oltage),"))
print(clgrn.format("                          S(ignal), M(sg delta), U(ltrasound), L(ight), C(osm), (Se)N(.se)"))
print(clgrn.format("                          (Lite)J(et demo)"))

#loop reading from serial port and from keyboard
looping = True
while looping:
    recmsg = rb.get_sensor_data(PACKETLEN) #see what's been sent (if anything)
    currmsgt = datetime.datetime.now() #mark the receive time
    if len(recmsg) == PACKETLEN: #if we got the right amount of data
        #parse the incoming data
        #TODO: rb.parse() should look for valid StartFrame (0xFE) and EndFrame (0xFF) values and return an error value
        rtsequence, rtid, rtxaxis, rtyaxis, rtzaxis, rtaccstat, rttemperature, rtbatvolt, rtrssi, rtus, rtlight = rb.parse(recmsg)

        
        rt[rtid].currmsgtime = currmsgt
        
        #accelerometer stati 
        rtaccorient = int(rtaccstat / 256) #separate our orientation bits from the rt status bits
        rtaccstat -= (rtaccorient * 256) #separate the status bits from the orientation bits
        rtaccbit = razstuff.get_bits(rtaccstat) #for displaying on console
        rtorientbit = razstuff.get_bits(rtaccorient) #for displaying on console
        
        #check for a gap in the sequence number
        gap = rt[rtid].seq.check(rtsequence)
        if gap > 0:
            gapmsg = clred.format("JUST MISSED SOME MESSAGES")
            rt[rtid].lastgaptime = str(rt[rtid].currmsgtime)
        else:
            gapmsg = ""
            
        #average the recent values
        rt[rtid].av.add(rtbatvolt) #average voltage
        rt[rtid].ar.add(rtrssi)  #average signal strength
        rt[rtid].ax.add(rtxaxis) #average the x axis values
        rt[rtid].ay.add(rtyaxis) #average the y axis values
        rt[rtid].az.add(rtzaxis) #average the z axis values
        rt[rtid].ast.add(rtaccstat) #save the accstat values to watch for change
        rt[rtid].ao.add(rtaccorient) #save the orientation values to watch for change
        rt[rtid].at.add(rttemperature) #average temperature
        rt[rtid].au.add(rtus) #average temperature
        rt[rtid].al.add(rtlight) #average temperature
        
        msgdelta = rt[rtid].currmsgtime - rt[rtid].prevmsgtime #time between messages
        mdmicro = msgdelta.microseconds #convert it to microseconds
        rt[rtid].ad.add(mdmicro) #averaging time between messages
        
        rt[rtid].prevmsgtime = rt[rtid].currmsgtime #save the time of this message
        rt[rtid].msgcount += 1 #count it
        
###########################################################
# LJ DIMMING DEMO
# send the command to the litejet monitor and ignore replies
        if ljdemo:
            #print("rtid=" + rtid + ", ljrtid=" + ljrtid)
            if rtid == ljrtid:
                xljdim = int(round(float(rtxaxis * 1.5),1)) 
                yljdim = int(round(float(rtyaxis * 1.5),1))

                tellit = "@LJDIM:" + str(ljxload).zfill(3) + "," + str(xljdim).zfill(2)
                tell_ljmon(tellit)
                #print("Telling LJMON [" + tellit + "]")
            
                tellit = "@LJDIM:" + str(ljyload).zfill(3) + "," + str(yljdim).zfill(2)
                tell_ljmon(tellit)
                #print("Telling LJMON [" + tellit + "]")
###########################################################

        if showpacket:
            print(cwht.format(rtid) + " " + cbrn.format(rt[rtid].rtname) + ": " + cwht.format(str(rtsequence)) + "/" + cwht.format(str(rt[rtid].msgcount)) + ": " + str(rt[rtid].currmsgtime) + " (" + str(msgdelta) + ")")

        if showraw:
            rawstr = recmsg.encode("hex")
            rawstr = cwht.format(rawstr[0:2]) + cgry.format(rawstr[2:4]) + cwht.format(rawstr[4:6]) + \
                        clgrn.format(rawstr[6:8]) + clred.format(rawstr[8:12]) + clcyn.format(rawstr[12:14]) + \
                        clmag.format(rawstr[14:16]) + cyel.format(rawstr[16:18]) + clgrn.format(rawstr[18:20]) + \
                        cwht.format(rawstr[20:22]) + cred.format(rawstr[22:26]) + cgrn.format(rawstr[26:30]) + \
                        clcyn.format(rawstr[30:32]) + cmag.format(rawstr[32:36]) + cyel.format(rawstr[36:38]) + cwht.format(rawstr[38:])              
            print(" [" + rawstr + "]") 
            
        if showtemp:
            avgtemp = rt[rtid].at.average(1) #get the average temperature so we can display it on the console
            print(" Temperature recent average=" + cgrn.format(str(avgtemp)) + ", immediate=" + cyel.format(str(rttemperature)))
            if rt[rtid].at.maximum > 0:
                print("  Max=" + cred.format(str(rt[rtid].at.maximum)) + " at " + str(rt[rtid].at.maxtime) + "  Min=" + ccyn.format(str(rt[rtid].at.minimum)) + " at " + str(rt[rtid].at.mintime) +  ")")
                
        if showgap:
            print(" " + str(rt[rtid].seq.gaps) + " gaps, last gap " + rt[rtid].lastgaptime + " " + gapmsg)
            
        if showaxis:
            print("Immediate: " + clcyn.format(" X:" + str(rtxaxis)) + ", " + cmag.format("Y:" + str(rtyaxis)) + ", " + cyel.format("Z:" + str(rtzaxis)))
            print("Average:   " + clcyn.format(" X:" + str(int(rt[rtid].ax.average(0)))) + ", " + cmag.format("Y:" + str(int(rt[rtid].ay.average(0)))) + ", " + cyel.format("Z:" + str(int(rt[rtid].az.average(0)))))
            print("  Status bits: " + " ".join(rtaccbit))
            print("  Orientation: " + " ".join(rtorientbit))
            
        if showvolt:
            print(" Battery: " + cred.format(str(rtbatvolt)))
            
        if showsig:
            print(" RSSI: " + cyel.format(str(rtrssi)))
            
        if showmd:
            print(" Average message delta: " + cmag.format(str(round(float(rt[rtid].ad.average(3)/1000000.0),3))))
            
        if showus:
            print(" Ultrasound: " + str(rtus))
            
        if showlight:
            print(" Light: " + str(rtlight))
            
        #note: post to Cosm and Sense only after displaying values on console because after posting we clear the averages. . .
        if postToCosm or postToSense: #if we're posting to either Cosm or Sense
            if currmsgt > rt[rtid].nextiotpost: # and it's time to post
                if postToCosm:
                    print(cgrn.format("Sending data to COSM for ") + clgrn.format(rtid + " " + rt[rtid].rtname) + " (" + str(currmsgt - rt[rtid].previotpost) + ")") 
                    rtdata = []
                    rtdata.append((asname, str(rtaccstat))) #accel status value is immediate, not averaged
                    rtdata.append((aoname, str(rtaccorient))) #accel orientation value is immediate, not averaged
                    if rt[rtid].at.average(1) > -50: #only post temperature if it's above 50-below zero (low battery will give a false reading!)
                        rtdata.append((tpname, str(rt[rtid].at.average(1)))) 
                    rtdata.append((btname, str(rt[rtid].av.average(2))))
                    rtdata.append((rsname, str(rt[rtid].ar.average(0))))
                    rtdata.append((mdname, str(round(float(rt[rtid].ad.average(3)/1000000.0),3))))
                    rtdata.append((usname, str(int(rt[rtid].au.average(0)))))
                    rtdata.append((ltname, str(int(rt[rtid].al.average(0)))))
                    
                    #if the axis values are contiguous (means it's probably not moving), send the average value, 
                    # otherwise send the immediate value (we'll send the immediate values a little later in the code)
                    if rt[rtid].ax.isContig():
                        rtdata.append((axname, str(int(rt[rtid].ax.average(0)))))
                    if rt[rtid].ay.isContig():
                        rtdata.append((ayname, str(int(rt[rtid].ay.average(0)))))
                    if rt[rtid].az.isContig():
                        rtdata.append((azname, str(int(rt[rtid].az.average(0)))))

                    rt[rtid].cosm.post(rtdata) #send the data to Cosm

                if postToSense:
                    #only send data when we have a feed number
                    rtdata = []
                    if rt[rtid].fs != "0":
                        rtdata.append((rt[rtid].fs, str(rtaccstat))) #accel status value is immediate, not averaged
                    if rt[rtid].fo != "0":
                        rtdata.append((rt[rtid].fo, str(rtaccorient))) #accel orientation value is immediate, not averaged
                    if rt[rtid].ft != "0":
                        if rt[rtid].at.average(1) > -50: #only post temperature if it's above 50-below zero (low battery will give a false reading!)
                            rtdata.append((rt[rtid].ft, str(rt[rtid].at.average(1))))
                    if rt[rtid].fv != "0":
                        rtdata.append((rt[rtid].fv, str(rt[rtid].av.average(2))))
                    if rt[rtid].fr != "0":
                        rtdata.append((rt[rtid].fr, str(rt[rtid].ar.average(0))))
                    if rt[rtid].fd != "0":
                        rtdata.append((rt[rtid].fd, str(round(float(rt[rtid].ad.average(3)/1000000.0),3))))
                    if rt[rtid].fu != "0":
                        rtdata.append((rt[rtid].fu, str(int(rt[rtid].au.average(0)))))
                    if rt[rtid].fl != "0":
                        rtdata.append((rt[rtid].fl, str(int(rt[rtid].al.average(0)))))
                        
                    #if the axis values are contiguous (probably not moving), send the average value, otherwise send the immediate value (a little later in the code)
                    if rt[rtid].fx != "0":
                        if rt[rtid].ax.isContig():
                            rtdata.append((rt[rtid].fx, str(int(rt[rtid].ax.average(0)))))
                    if rt[rtid].fy != "0":
                        if rt[rtid].ay.isContig():
                            rtdata.append((rt[rtid].fy, str(int(rt[rtid].ay.average(0)))))
                    if rt[rtid].fz != "0":
                        if rt[rtid].az.isContig():
                            rtdata.append((rt[rtid].fz, str(int(rt[rtid].az.average(0)))))

                    if len(rtdata) > 0: #if at least one item to be posted
                        print(cgrn.format("Sending data to sen.se for ") + clgrn.format(rtid + " " + rt[rtid].rtname) + " (" + str(currmsgt - rt[rtid].previotpost) + ")") 
                        rt[rtid].sense.post(rtdata) #send the data to Sen.se
                        
                #set up the next time to post to Cosm and Sense
                rt[rtid].nextiotpost = currmsgt + datetime.timedelta(seconds=rt[rtid].rate) 
                rt[rtid].previotpost = currmsgt

                #reset all the averaging values so we get new values for the next post
                rt[rtid].av.clear()
                rt[rtid].ar.clear()
                rt[rtid].at.clear()
                rt[rtid].ad.clear()
                rt[rtid].ax.clear()
                rt[rtid].ay.clear()
                rt[rtid].az.clear()
                rt[rtid].ast.clear()
                rt[rtid].ao.clear()
                rt[rtid].au.clear()
                rt[rtid].al.clear()

            #now we'll look to see if the accelerometer is detecting movement
            #changes in accel value of 0 or 1 are considered stationary
            # potentially, this means that if the RT moves very very slowly, e.g. 1 accel unit at a time, 
            # no movement will be detected -- reasonably unlikely.
            #if movement is detected (any axis value has changed by more than 1 unit), we'll post the data to Cosm
            #TODO: This may cause a data rate issue at Cosm or Sense.  Need to find a better way.
            rtcdata = []
            rtsdata = []
            if not rt[rtid].ax.isContig():
                rtcdata.append((axname, str(rtxaxis)))
                if rt[rtid].fx != "0":
                    rtsdata.append((rt[rtid].fx, str(rtxaxis)))
            if not rt[rtid].ay.isContig():
                rtcdata.append((ayname, str(rtyaxis)))
                if rt[rtid].fy != "0":
                    rtsdata.append((rt[rtid].fy, str(rtyaxis)))
            if not rt[rtid].az.isContig():
                rtcdata.append((azname, str(rtzaxis)))
                if rt[rtid].fz != "0":
                    rtsdata.append((rt[rtid].fz, str(rtzaxis)))
            if rt[rtid].ast.isDifferent():
                rtcdata.append((asname, str(rtaccstat))) 
                if rt[rtid].fs != "0":
                    rtsdata.append((rt[rtid].fs, str(rtaccstat)))
            if rt[rtid].ao.isDifferent():
                rtcdata.append((aoname, str(rtaccorient))) 
                if rt[rtid].fo != "0":
                    rtsdata.append((rt[rtid].fo, str(rtaccorient)))
            if len(rtcdata) > 0: #if any of the values
                print(clcyn.format("Sending " + str(len(rtcdata)) + " axis data values to COSM from ") + ccyn.format(rtid + " " + rt[rtid].rtname)) 
                rt[rtid].cosm.post(rtcdata) #send the data to Cosm

    else: #we didn't get the right amount of data
        if len(recmsg) > 0:
            print(cbrn.format("Skipping short data payload [" + str(len(recmsg)) + "]"))

    #post the weather to Cosm and Sense if it's time. . .
    if postToCosm or postToSense:
        if currmsgt > wx.nextiotpost:
            razstuff.get_local_weather(wx, postToCosm, postToSense)
            wx.nextiotpost = currmsgt + datetime.timedelta(seconds=wx.rate) #set up the next post time

    kbd = razstuff.kbd_ready() #see if anything from the keyboard (<enter> was pressed)
    if len(kbd) > 0: 
        choice = kbd[:1].upper() #look at only the first character in upper-case
   
        if choice == "X": #X exits the program
            looping = False #exit the loop, thereby exiting the program (after cleanup)
        elif choice == "P": #P toggles showing packet data
            showpacket = not showpacket
        elif choice == "R": #R toggles showing raw data
            showraw = not showraw
        elif choice == "T": #T toggles showing temperature
            showtemp = not showtemp
        elif choice == "G": #G toggles showing gaps
            showgap = not showgap
        elif choice == "A": #A toggles showing accel/axis data
            showaxis = not showaxis
        elif choice == "V": #V toggles showing voltage
            showvolt = not showvolt
        elif choice == "S": #S toggles showing signal strength
            showsig = not showsig
        elif choice == "M": #M toggles showing message delta average
            showmd = not showmd
        elif choice == "L": #L toggles showing message delta average
            showlight = not showlight
        elif choice == "U": #U toggles showing message delta average
            showus = not showus
        elif choice == "C": #C toggles posting to Cosm
            postToCosm = not postToCosm
            if postToCosm:
                print(clgrn.format("Resuming posting to Cosm"))
            else:
                print(clred.format("Suspending posting to Cosm"))
        elif choice == "N": #N toggles posting to Sen.se
            postToSense = not postToSense
            if postToSense:
                print(clgrn.format("Resuming posting to Sen.se"))
            else:
                print(clred.format("Suspending posting to Sen.se"))
###########################################################
# LJ DIMMING DEMO
        elif choice == "J": #J toggles the LiteJet dimming demo 
            ljdemo = not ljdemo
            if ljdemo:
                print(clgrn.format("*************** L J D E M O    A C T I V E ***************"))
                start_ljdemo()
            else:
                print(clred.format("*************** L J D E M O    S T O P P E D ***************"))
                stop_ljdemo()
###########################################################

# END OF while looping: MAIN PROGRAM LOOP

byebye()            
