razb_sensors.py
ztc.py

put both files in the same directory

be sure to first install the python-rpi.gpio package with this command:
    sudo apt-get install python-rpi.gpio
    
run the program with this command:
    sudo python razb_sensors.py
    
while the program is running:
pressing X<enter> will terminate the loop (and the program)
pressing R<enter> will toggle display of the raw data
pressing T<enter> will toggle display of the temperature data
pressing G<enter> will toggle display of the gap info

