import select
import serial
import string
        
class ZtcMsg :
    
    # open and initialize the serial port
    # the speed is specified by the caller
    def __init__(self, speed):
        self.ser = serial.Serial("/dev/ttyAMA0")
        self.ser.baudrate = speed
        self.ser.bytesize = 8
        self.ser.parity = "N"
        self.ser.stopbits = 1
        self.ser.timeout = 1
        self.ser.xonxoff = 0
        self.ser.rtscts = 0
        print("Opened serial port " + self.ser.portstr + " @ " + str(self.ser.baudrate))
        self.ztc_send_raw("\xfd\x07")
        
    # ztc_close closes the serial port (bonus points if you figured that out yourself)
    def ztc_close(self):
        self.ser.close()
        print("Closed serial port")

        
    # ztc_send sends a binary message to the serial port
    #  adding the 0x02 header and crc to the passed message
    def ztc_send(self, ztc_msg):
        crc = self.ztc_crc(ztc_msg) #calculate the CRC
        msg = chr(2) + ztc_msg + chr(crc) #slap a header and crc on the message
        print(" full msg [" + msg.encode("hex") + "]")
        self.ser.write(msg)
        print("Sent message[" + msg.encode("hex") + "]") #we're done

    # ztc_send sends a binary message to the serial port
    #  adding the 0x02 header and crc to the passed message
    def ztc_send_raw(self, ztc_msg):
        msg = ztc_msg 
        self.ser.write(msg)
        print("Sent raw data [" + msg.encode("hex") + "]") #we're done
        
        
    # ztc_recv reads at most a requested number of bytes from the serial port
    def ztc_recv(self, length):
        while 1: 
            response = self.ser.read(length)
            if len(response) > 0:
                break;
        return response

        
    # ztc_get_msg calls ztc_recv only 3 times for a full record
    #  first reads 1 byte for the 0x02 start byte
    #  second reads 3 bytes go get the opcode group, opcode, and length
    #  third reads the data (based on the length from the prior read) plus the crc
    # to read byte-by-byte, see ztc_get_msg_bytebybyte
    def ztc_get_msg(self):
        i,o,e = select.select([self.ser],[],[],0.0001)
        for s in i:
            if s == self.ser:
                ztcmsg = ""
                msglen = 0
                datalen = 0
                recsize = 1
                while 1:
                    inbytes = self.ztc_recv(recsize)
                    if msglen == 0:
                        if inbytes <> chr(2): #first byte must be 0x02
                            return ""
                        else:
                            msglen = 1
                            recsize = 3 #read the opcode group, opcode, and data length
                            ztcmsg = inbytes
                            continue
                            
                    if msglen == 1: #should have received 3 bytes
                        if len(inbytes) <> 3:
                            return "" #we failed, return nothing
                        ztcmsg += inbytes #add the byte to the message
                        msglen = 4
                        recsize = ord(inbytes[-1]) + 1 #get the data plus the crc byte
                        continue
                    
                    if msglen == 4:
                        if len(inbytes) <> (recsize): #if we didn't get all the data
                            return "" #we failed, so return nothing
                        ztcmsg += inbytes
                        return ztcmsg #return the full message

        return "" #no serial input available

        
    # ztc_get_msg_bytebybyte gets does a recv for each byte 
    #  this is old, less efficient code than ztc_get_msg
    def ztc_get_msg_bytebybyte(self):
        i,o,e = select.select([self.ser],[],[],0.0001)
        for s in i:
            if s == self.ser:
                ztcmsg = ""
                msglen = 0
                datalen = 0
                while 1:
                    inbyte = self.ztc_recv(1)
                    if msglen == 0 and inbyte <> chr(2): #first byte must be 0x02
                        continue
                    msglen += 1 #number of bytes rec'd for this message
                    ztcmsg += inbyte #add the byte to the message
                    if msglen == 4: #if this is the fourth byte it's the data length
                        datalen = ord(inbyte) #save the data length
                    if msglen > 4: #if we're in the data
                        if datalen == 0: #and we've read it all
                            return ztcmsg #return the message
                        datalen -= 1 #count down the bytes of data
        return "" #no serial input available


    #try to get the 17 bytes of sensor data
    def ztc_getsensors(self):
        msg = ""
        i,o,e = select.select([self.ser],[],[],0.0001)
        for s in i:
            if s == self.ser:
                msg = self.ztc_recv(17)
                        
        return msg


        
    def ztc_crc(self, msg):
        crc = 0
        for y in range(0, len(msg), 1):
            onebyte =  msg[y]
            crc ^= ord(onebyte)
        return crc

   
# END of class ZTC
