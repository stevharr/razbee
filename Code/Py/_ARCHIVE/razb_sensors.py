#!/usr/bin/env python

#razb_sensors
# display incoming remote sensor data from a RazBee

from __future__ import print_function

import base64
import datetime
import RPi.GPIO as GPIO
import select
import string
import sys
import time
import ztc #this is my module 

class RazBParse: #parse the sensor data coming from a RazBee
    
    def __init__(self):
        pass
    
    def parse_rx(self, msg):
        #converts the data from binary to ascii-hex, then converts the values back to binary
        #this isn't the most efficient for dealing with the data for display by this program, but 
        # since it will end up sending the data somewhere else (probably XML), then this is probably ok
        #some values, like temperature, need some extra attention to decode them properly
        msgasc = msg.encode("hex") #convert the data to ascii-hex

        #get the individual data fields from the ascii-hex string
        #TO DO: id, etc.
        hexsequence = msgasc[6:8]
        hexwinddir = msgasc[10:14]
        hexwindspeed = msgasc[14:18]
        hexpressure = msgasc[18:22]
        hextempmsb = msgasc[22:24]
        hextemplsb = msgasc[24:26]
        hexvbat = msgasc[26:30]
        
        #for internal use by this program, convert to binary values
        sequence = self.hex2dec(hexsequence)
        winddir = self.hex2dec(hexwinddir)
        windspeed = self.hex2dec(hexwindspeed)
        pressure = self.hex2dec(hexpressure)
        temperature = self.makefloat(hextempmsb, hextemplsb)
        temperature = self.ctof(temperature)
        vbat = self.hex2dec(hexvbat)
        return sequence, winddir, windspeed, pressure, temperature, vbat 

    def makefloat(self, ahmsb, ahlsb): #convert two bytes to a float: msb is signed integer part, lsb is decimal part
        binmsb = self.hex2dec(ahmsb)
        binlsb = self.hex2dec(ahlsb)
        if binmsb > 127:
            binmsb -= 128
            sign = "-"
        else:
            sign = ""
        floatval = float(sign + str(binmsb) + "." + str(binlsb))
        return floatval
        
    def ctof(self, celsius): #convert celcius to fahrenheit
        return float((celsius * 1.8) + 32) #convert to fahrenheit
        
    def hex2dec(self, s):  #return the integer value of a hexadecimal string (ie, "0D" returns 13)
        return int(s, 16)


        
class AvgList:  #creates a rolling list, tracking the last X values and calculating the average of those values

    def __init__(self, length): #create a list of the requested length, init w/zeroes
        self.len = length
        self.rlist = [0]*self.len
        self.count = 0
        self.ready = False
        
    def add(self, newval): #add an item to front of the list, removing the last item from the list
        self.rlist.pop()
        self.rlist.insert(0, newval)
        if self.ready == False: #if we haven't filled the entire list with user values
            if self.count < self.len: 
                self.count += 1
            else:
                self.ready = True

    def is_ready(self):  #returns True when all items in the list contain user data
        return self.ready
        
    def average(self): #returns the average (as a float) of all the items in the list
        self.avgtemp = float(sum(self.rlist))/float(len(self.rlist))
        self.avgtemp = float((int(self.avgtemp * 10.0)) / 10.0)
        return self.avgtemp

        
class SeqCheck: #verifies that consecutive calls to check() are contiguous values without gaps
    def __init__(self, max):
        self.seqmax = max
        self.shouldbe = self.seqmax + 1
        self.init = False
        self.gaps = 0

    def check(self, got): #
        if self.init == True: #if we've received an initial value
            retval = got - self.shouldbe #should be zero if there's no gap in the sequence
            if retval != 0:
                self.gaps += 1 #count the number times we've had gaps in sequences
        else:
            self.init = True #we just got our initial value
            retval = 0 #so the return value is 0 ("no gap")
        self.shouldbe = got + 1 #the next number "should be" the current value plus one
        if self.shouldbe > self.seqmax: #if the next number exceeds the rollover value
            self.shouldbe = 0 #reset the sequence number
        return retval
        
    def gaps(self): #returns the number of times we've had gaps in the sequence
        return self.gaps
        
    
def kbd_ready(): # returns true if there's data from the keyboard (ending with <enter>)
    i,o,e = select.select([sys.stdin],[],[],0.0001)
    for s in i:
        if s == sys.stdin:
            input = sys.stdin.readline()
            return input #return what was entered
    return "" #return empty string if no keyboard input

def resetRazbeeRadio(): #Reset the MC1322x radio chip on the RazBee 
    pin = 18 #GPIO-1 is RPi pin 18 
    value = 0
    states = [GPIO.HIGH, GPIO.HIGH, GPIO.LOW, GPIO.HIGH, GPIO.HIGH]
    print("Resetting MC1322x Radio Chip on RazBee")
    #Set the mode numbering of the pins
    GPIO.setmode(GPIO.BCM)

    GPIO.setup(pin, GPIO.IN)
    value = GPIO.input(pin)
    print("Beginning state of GPIO-x (pin = " + str(pin) + ") is " + str(value))
    
    for state in states:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, state)
        # GPIO.setup(pin, GPIO.IN)
        value = GPIO.input(pin)
        print("Current state of GPIO-x (pin = " + str(pin) + ") is " + str(value))
        time.sleep(1)

   
### PROGRAM BEGINS HERE ###

print("Starting " + str(datetime.datetime.now()))

#color output
cblk = "\033[22;30m{0}\033[00m" # black
cdgry = "\033[01;30m{0}\033[00m" # dark gray
cgry = "\033[22;37m{0}\033[00m" # gray
cwht = "\033[01;37m{0}\033[00m" # white

cbrn = "\033[22;33m{0}\033[00m" # brown

cred = "\033[22;31m{0}\033[00m" # red
cgrn = "\033[22;32m{0}\033[00m" # green
cblu = "\033[22;34m{0}\033[00m" # blue
clred = "\033[01;31m{0}\033[00m" # light red
clgrn = "\033[01;32m{0}\033[00m" # light green
clblu = "\033[01;34m{0}\033[00m" # light blue

ccyn = "\033[22;36m{0}\033[00m" # cyan
cmag = "\033[22;35m{0}\033[00m" # magenta
cyel = "\033[01;33m{0}\033[00m" # yellow
clcyn = "\033[01;36m{0}\033[00m" # light cyan
clmag = "\033[01;35m{0}\033[00m" # light magenta

#to use colors: cxxx.format("text to be in color")


prevmsgtime = datetime.datetime.now() #we'll use these to calculate time between messages
currmsgtime = prevmsgtime
    
looping = 1
msgcount = 0
mintemp = 999
maxtemp = -999
minttime = prevmsgtime
maxttime = prevmsgtime
prevtemp = 999
temptrend = ""

resetRazbeeRadio() #reset the MC1322x radio chip on the RazBee

z = ztc.ZtcMsg(38400) #open the serial port
razb = RazBParse() #initialize the parser
at = AvgList(10) #create a list to track and average the last 10 temperature readings
seq = SeqCheck(255) #to check for consecutive sequence numbers    
lastgaptime = "never"

#In the main loop:    
# pressing X<enter> will terminate the loop (and the program)
# pressing R<enter> will toggle display of the raw data
# pressing T<enter> will toggle display of the temperature data
# pressing G<enter> will toggle display of the gap info
#  set defaults here:
showraw = True
showtemp = True
showgap = True

print("And away we go!")
print("Press X<enter> to exit. . .")

#loop reading from serial port and from keyboard
while looping == 1:
    recmsg = z.ztc_getsensors() 
    if len(recmsg) > 0: 
        currmsgtime = datetime.datetime.now()
        rbsequence, rbwinddir, rbwindspeed, rbpressure, rbtemperature, rbvbat = razb.parse_rx(recmsg)

        gap = seq.check(rbsequence)
        if gap > 0:
            gapmsg = "JUST MISSED SOME MESSAGES"
            lastgaptime = str(currmsgtime)
        else:
            gapmsg = ""
            
        at.add(rbtemperature)
        
        if at.is_ready() == True:
            maxmin = ""
            avgtemp = at.average()
            if avgtemp > maxtemp:
                maxtemp = avgtemp
                maxttime = currmsgtime
                if prevtemp !=999:
                    maxmin = cred.format(" NEW HIGH")
            if avgtemp < mintemp:
                mintemp = avgtemp
                minttime = currmsgtime
                if prevtemp !=999:
                    maxmin = ccyn.format(" NEW LOW")
            if avgtemp > prevtemp:
                if prevtemp != 999:
                    temptrend = cred.format("UP " + str(avgtemp - prevtemp))
            if avgtemp < prevtemp:
                if prevtemp != 999:
                    temptrend = ccyn.format("DOWN " + str(avgtemp - prevtemp))
            if avgtemp == prevtemp:
                temptrend = ""

            prevtemp = avgtemp
        else:
            avgtemp = "being calculated"
            temptrend = ""
            maxmin = ""
       
        msgdelta = currmsgtime - prevmsgtime
        prevmsgtime = currmsgtime
        msgcount += 1 #count it

        if showraw:
            print(str(rbsequence) + "/" + str(msgcount) + ": " + str(datetime.datetime.now()) + " (" + str(msgdelta) + ") " + " [" + recmsg.encode("hex") + "]") #and display it
        if showtemp:
            print(" Temperature recent average=" + cgrn.format(str(avgtemp)) + ", immediate=" + cyel.format(str(rbtemperature)) + "  " + temptrend)
            if maxtemp > 0:
                print("  Max=" + cred.format(str(maxtemp)) + " at " + str(maxttime) + "  Min=" + ccyn.format(str(mintemp)) + " at " + str(minttime) +  ")" + maxmin)
        if showgap:
            print("  " + str(seq.gaps) + " gaps, last gap " + lastgaptime + " " + gapmsg)
        
    kbd = kbd_ready() #see if anything from the keyboard
    if len(kbd) > 0: 
        choice = kbd[:1]
        if choice == "X" or choice == "x": #X exits the program
            looping = 0 #exit the loop, thereby exiting the program (after cleanup)
        elif choice == "R" or choice == "r": #R toggles showing raw data
            showraw = not showraw
        elif choice == "T" or choice == "t": #T toggles showing temperature
            showtemp = not showtemp
        elif choice == "G" or choice == "g": #G toggles showing gaps
            showgap = not showgap

z.ztc_close() #close the serial port
print("Done. . .")
