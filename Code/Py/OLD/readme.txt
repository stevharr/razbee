put these files in the same directory:
    razb_sensors.py
    ztc.py
    razb_sensors.xml

install necessary packages - run these commands, in this order (copy and paste for best results):
    sudo apt-get install python-serial
    sudo apt-get install python-rpi.gpio
    sudo apt-get install python-pip
    wget -O geekman-python-eeml.tar.gz https://github.com/geekman/python-eeml/tarball/master
    tar zxvf geekman-python-eeml.tar.gz
    cd geekman-python-eeml****** (check for the actual directory name with ls -l)
    sudo python setup.py install

edit the razb_sensors.xml file with your own Cosm api-key and feed number
    you MUST enter these values!
    
run the program with this command:
    sudo python razb_sensors.py
    
data will be posted to Cosm!
    
while the program is running:
    DON'T PRESS ^C TO TERMINATE THE PROGRAM!  USE X<ENTER>
    pressing X<enter> will terminate the loop (and the program)
    pressing R<enter> will toggle display of the raw data
    pressing T<enter> will toggle display of the temperature data
    pressing G<enter> will toggle display of the gap info
    pressing C<enter> will toggle logging to Cosm
    pressing A<enter> will toggle display of the axis info
    pressing V<enter> will toggle display of the voltage info
    pressing S<enter> will toggle display of the signal strength info

