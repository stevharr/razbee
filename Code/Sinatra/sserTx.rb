require 'sinatra'
require 'serialport'

class IOStreamTx
   def initialize
     #params for serial port
     port_str = "/dev/ttyAMA0"
     baud_rate = 38400
     data_bits = 8
     stop_bits = 1
     parity = SerialPort::NONE
     @sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
   end

   def each
      i = 0
      while true do
         @sp.putc('0')
         sleep(1)
         yield "TX:#{i}\n"
         i = i + 1
      end
   end
end

get ('/') { IOStreamTx.new } 
