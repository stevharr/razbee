/*
 *  hexdump.c
 *  ZBoot
 *
 *  Created by Steve on 9/15/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "hexdump.h"

static eDUMP_FORMAT_T myFormat;
static int myValuesPerRow;
static int myOptions;

void dump_header(eDUMP_FORMAT_T format, int values_per_row, int options)
{
	int i;
	
	// store for later use during writing
	myOptions = options;
	myValuesPerRow = values_per_row;
	myFormat = format;
	
	printf("\n\t ");
	switch (format) 
	{
		case _BYTE:
			if (values_per_row > MAX_BYTES_PER_ROW) {
				values_per_row = MAX_BYTES_PER_ROW;
			}
			for(i=0; i < values_per_row; i++)
			{
				printf("%01x  ",i);
				if (i == values_per_row/2-1) printf("  ");
			}

			if (options == _ASCII) {
				printf("\t---ASCII DATA---\n\t");
			}
			for(i=0; i < values_per_row; i++)
			{
				printf("-- ");
				if (i == values_per_row/2-1) printf("  ");
			}
			
			printf("\t");
			for(i=0; i < values_per_row; i++)
			{
				printf("-");
			}
			break;

		case _WORD:
			if (values_per_row > MAX_WORDS_PER_ROW) 
			{
				values_per_row = MAX_WORDS_PER_ROW;
			}
			
			for(i=0; i < values_per_row; i++)
			{
				printf("%01x  ",i);
				if (i == values_per_row/2-1) printf("  ");
			}
			
			if (options == _ASCII) {
				printf("\t---ASCII DATA---\n\t");
			}
			
			for(i=0; i < values_per_row; i++)
			{
				printf("-- ");
				if (i == values_per_row/2-1) printf("  ");
			}
			
			printf("\t");
			for(i=0; i < values_per_row; i++)
			{
				printf("-");
			}
			break;
			
		case _LONG:
			if (values_per_row > MAX_LONGS_PER_ROW) 
			{
				values_per_row = MAX_LONGS_PER_ROW;
			}
			for(i=0; i < values_per_row; i++)
			{
				printf("%01x  ",i);
				if (i == values_per_row/2-1) printf("  ");
			}
			
			if (options == _ASCII) 
			{
				printf("\t---ASCII DATA---\n\t");
			}
			
			for(i=0; i < values_per_row; i++)
			{
				printf("-- ");
				if (i == values_per_row/2-1) printf("  ");
			}
			printf("\t");
			for(i=0; i < values_per_row; i++)
			{
				printf("-");
			}
			
			break;
		default:
			break;
	}
	printf("\n");
	
}

void dump_row()
{
	
}

