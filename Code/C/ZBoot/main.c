#include <stdio.h>
#include "hexdump.h"

int main (int argc, const char * argv[]) {
	
    FILE * pF;
    int c;
    int n=0;
		
    // filename?
    if(argc < 2)
    {
       printf("USAGE: ZBoot filename\n");
       return 1;
    }

    // open binary processor file
    pF = fopen (argv[1],"rb");
    if(pF)
    {
    } else {
    printf("Failed to open %s", argv[1]);
    return(1);
    }

	if (pF==NULL) {
		perror ("Error opening file");
	}
	else {
		do 
		{
			c = fgetc (pF);
			printf("%d-%x\n",n,c);
			n++;
		} while (c != EOF);
		fclose (pF);
		printf ("File: [%s] contains %d bytes.\n",argv[1],n);	
	}
	dump_header(_BYTE, 16, _ASCII);
	
    return 0;
}
