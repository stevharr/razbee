/*
 *  hexdump.h
 *  ZBoot
 *
 *  Created by Steve on 9/15/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include <stdio.h>

#define	MAX_BYTES_PER_ROW	32
#define	MAX_WORDS_PER_ROW	16
#define	MAX_LONGS_PER_ROW	8

typedef enum {
	_BYTE	=0x01,		// 00 00 00 00...
	_WORD	=0x02,		// 0000 0000 0000 0000...
	_LONG	=0x04,		// 00000000 00000000 00000000...
	_ASCII	=0x80		// flag for writing out ascii chars
} eDUMP_FORMAT_T;

void dump_header(eDUMP_FORMAT_T, int values_per_row, int options);

