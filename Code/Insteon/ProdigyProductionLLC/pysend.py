# Note: Set PLM to sync mode first followed by device sync
# Commands list: http://www.madreporite.com/insteon/commands.htm

import serial, time

print "Opening Serial Port"
ser = serial.Serial(
                    port='/dev/ttyUSB0', #'/dev/ttyUSB0'
                    baudrate=19200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=0
                    )
ser.flushInput()
ser.flushOutput()
time.sleep(1)


print "Sending Test OFF Message"
message = bytearray()
message.append(0x02) # INSTEON_PLM_START
message.append(0x62) # INSTEON_STANDARD_MESSAGE
# device id
message.append(0x04) # Addr 1
message.append(0x9B) # Addr 2
message.append(0xC2) # Addr 3
message.append(0x0F) # INSTEON_MESSAGE_FLAG
message.append(0x12) # 0x12 = FAST ON, 0x14 = FAST OFF, 0x19 = STATUS
message.append(0x00) # 0x00 = 0%, 0xFF = 100%
ser.write(message)
ser.flush()

time.sleep(1)

print "Sending Test ON Message"
message = bytearray()
message.append(0x02) # INSTEON_PLM_START
message.append(0x62) # INSTEON_STANDARD_MESSAGE
# device id
message.append(0x04) # Addr 1
message.append(0x9B) # Addr 2
message.append(0xC2) # Addr 3
message.append(0x0F) # INSTEON_MESSAGE_FLAG
message.append(0x12) # 0x12 = FAST ON, 0x14 = FAST OFF, 0x19 = STATUS
message.append(0xFF) # 0x00 = 0%, 0xFF = 100%
ser.flush()
ser.close()
