#!/home/pi/.rvm/rubies/ruby-1.9.3-p194/bin/ruby

require 'wiringpi'
require "serialport"

#params for serial port
port_str = "/dev/ttyAMA0"
baud_rate = 115200 
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE
 
sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
 
pin = 1
value = 0
N = 1

io = WiringPi::GPIO.new

printf("Serial Baud:%d\n",baud_rate);
puts "Resetting MC1322x Radio Chip"
io.mode(pin,OUTPUT)
(0..N).each do |i|
   value = io.read(pin)
   puts "GPIO-" + pin.to_s + " = " + value.to_s
   io.write(pin,i & 0x01)
   sleep(1)
end

puts "Assert RTS - LED On = RTS Asserted"
pin = 0
io.mode(pin,OUTPUT)
(0..N).each do |i|
   value = io.read(pin)
   puts "GPIO-" + pin.to_s + " = " + value.to_s
   io.write(pin,i & 0x01)
   sleep(1)
end

#just read forever
puts "Read Serial Port Data"
(0..100).each do
#  sp.putc('1');
  s = sp.getc
#  puts s.to_i
  printf("[%x]", s.to_i)
end
puts
sp.close

