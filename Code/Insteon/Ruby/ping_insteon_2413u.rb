require "serialport"
require "byebug"

#params for serial port
port_str = "/dev/ttyUSB0"
baud_rate = 19200
data_bits = 8
stop_bits = 1
parity = SerialPort::NONE

d1 = ['04', '9B', 'C2'] ## lamp module that works (links)
d2 = ['04', '80', 'D7'] ## lamp module that doesn't seem to link

   d = d1
sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)

   message = [] 
   message << "0x02".to_i(16) # INSTEON_PLM_START
   message << "0x62".to_i(16) # INSTEON_STANDARD_MESSAGE
   # device id
   message << d[0].to_i(16) # Addr 1
   message << d[1].to_i(16) # Addr 2
   message << d[2].to_i(16) # Addr 3

   message << "0x0F".to_i(16) # INSTEON_MESSAGE_FLAG
   message << "0x10".to_i(16) # PING COMMAND 
   message << "0x00".to_i(16) # 0x00 = 0%, 0xFF = 100%

   ## =================== PING =====================
   printf("Read Test at %d baud\n",baud_rate);
   time_delay1 = 1.5
   j = 0
   100.times do
      puts "\n\nSending PING Command #{j} to Responder: #{d[0]}.#{d[1]}.#{d[2]}"
      message.each { |c| printf("[%.2X] ",c) }

      ## send command to PLC
      message.each { |x| sp.putc(x) }

      ## wait and then get reponse
      sleep(time_delay1)
      puts "\nResponse from PLC"
      i = 0
      while (i < 20) do
         cc = sp.getc
         if cc.nil?
            puts "nil"
         else
            x = cc.each_byte.first
            printf("[%.2X] ",x)
         end 
         i = i+1
      end

      ## wait and then get reponse
      # sleep(time_delay1)
      puts "\nResponse from PLC"
      i = 0
      while (i < 20) do
         cc = sp.getc
         if cc.nil?
            puts "nil"
         else
            x = cc.each_byte.first
            printf("[%.2X] ",x)
         end 
         i = i+1
      end

      ## wait and then get reponse
      # sleep(time_delay1)
      puts "\nResponse from PLC"
      i = 0
      while (i < 2) do
         cc = sp.getc
         if cc.nil?
            puts "nil"
         else
            x = cc.each_byte.first
            printf("[%.2X] ",x)
         end 
         i = i+1
      end

      # sleep(3)
      j = j + 1
   end
   sp.close
   puts 'Serial Port Closed'
