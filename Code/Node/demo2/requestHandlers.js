var exec = require("child_process").exec;

function start(response) {
   console.log("Request handler 'start' was called.");

   var content = "empty";
   exec("ls -lah", function (error, stdout, stderr) {
      response.writeHead(200, {"Content-Type": "text/plain"});
      response.write(stdout);
      response.end();
   });
}

function upload(response) {
   console.log("Request handler 'upload' was called.");
   var uploadTime = new Date().getTime();
   response.writeHead(200, {"Content-Type": "text/plain"});
   response.write("Hello Upload" + uploadTime);
   response.end();
}

exports.start = start;
exports.upload = upload;
