#!/home/pi/.rvm/rubies/ruby-1.9.3-p194/bin/ruby

require 'wiringpi'

pin = 1
value = 0
N = 3

io = WiringPi::GPIO.new

io.mode(pin,OUTPUT)

(0..N).each do |i|
   value = io.read(pin)
   puts "GPIO" + pin.to_s + " = " + value.to_s
   io.write(pin,i & 0x01)
   sleep(1)
end

pin = 0
io.mode(pin,OUTPUT)
(0..N).each do |i|
   value = io.read(pin)
   puts "GPIO" + pin.to_s + " = " + value.to_s
   io.write(pin,i & 0x01)
   sleep(1)
end
