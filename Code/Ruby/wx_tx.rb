#!/home/pi/.rvm/rubies/ruby-1.9.3-p194/bin/ruby
#==============================================================================
#      Program: wx_tx.rb
#  Description: RPi-->RazBee Simple Serial Transmit Utility for the Wx Station 
#       Inputs: $stdin 
#       Author: Steve Dame (sdame172@gmail.com)
#      Version: 0.1 
# Dependencies: http://rubygems.org/gems/serialport
# Prerequisite: gem install serialport
#==============================================================================
require "serialport"
require 'wiringpi'



#===================================ZtcMsg=====================================
# ----------------------------------------------------------------------------
#       Class: ZtcMsg
# Description: Sender class for Zigbee ZTC Messages
# ----------------------------------------------------------------------------
class ZtcMsg
  # --------------------------------------------------------------------------
  # constructor
  # --------------------------------------------------------------------------
  def initialize
    #params for serial port running at 38.4Kbaud
    port_str = "/dev/ttyAMA0"
    baud_rate = 38400
    data_bits = 8
    stop_bits = 1
    parity = SerialPort::NONE
    @sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
  end

  # --------------------------------------------------------------------------
  # Close the serial port 
  # --------------------------------------------------------------------------
  def close
    @sp.close
  end
  
  # --------------------------------------------------------------------------
  # Send a fully formed ZTC byte string command of the form
  #   "02 A3 08 00 AB"  or "02A30800AB"
  # --------------------------------------------------------------------------
  def ztc_send(ztc_full_msg)
    # strip space separators (if any)
    ztc = ztc_full_msg.gsub(' ','')
    puts ztc
    ztc.scan(/../).map do |x| 
       @sp.putc(x.hex) 
       puts x
    end
  end

  # --------------------------------------------------------------------------
  # compute the (lame) XOR checksum "CRC" for a series of ZTC command bytes
  #   note: must exclude the Sync (02) and any existing CRC (last byte) 
  #   Can be of the form: "A3 08 00"  or "A30800"
  # --------------------------------------------------------------------------
  def ztc_crc(msg)
    ztc = msg.gsub(' ','')
    puts ztc
    crc = 0
    ztc.scan(/../).map { |x| crc = crc ^ x.hex }
    puts crc.to_s(16).upcase
    return crc
  end
end
#===================================ZtcMsg=====================================

# Zigbee ZTC CPU Reset Request
ztc_rst = "FD 07"

# create a new message
z = ZtcMsg.new  
io = WiringPi::GPIO.new

pin = 1
value = 0
N = 2
S = [1,0,1]

puts "Resetting MC1322x Radio Chip on RazBee"
io.mode(pin,OUTPUT)
(0..N).each do |i|
   value = io.read(pin)
   puts "Current State of GPIO-" + pin.to_s + " = " + value.to_s
   io.write(pin,S[i])
   sleep(1)
end


puts "Send RazBee Wx Base Stattion MSG Loop"
i = 0;
while (i < 2) do
  z.ztc_send(ztc_rst)
  printf("%d\n",i);
  sleep(1)
  i = i+1
end
puts "Closing TX Connection"
z.close
